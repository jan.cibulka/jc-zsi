function X = myfft(x)

N = length(x);
if mod(log2(N),1) ~= 0
	error('myfft() expect 2^n elements in array!')
end

if N == 1
% 	X = x;
	X = x * exp(-2j*pi*1/N);
% 	X(1) = x(1) + x(2)
% 	X(2) = x(1) - x(2) * exp(-2i*pi/N)
else
	M = N/2;
% 	
% 	x_e = x(2:2:N);
% 	x_o = x(1:2:N);
	x_e = x(1:2:N);
	x_o = x(2:2:N);
	X_e = myfft(x_e);
	X_o = myfft(x_o);
	
	X = zeros(1, N);
	for n = 1:M
		X(n) = X_e(n) + X_o(n) * exp(2i*pi*(n)/N);
	end
	for n = M+1:N
		X(n) = X_e(n-M) - X_o(n-M) * exp(2i*pi*(n-M)/N);
	end
% 	size(X)
end
% X = X(end:-1:1);
end