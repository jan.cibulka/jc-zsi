function h = bandStop(fd, fh, Fs, M)

wd = 2*pi*fd/Fs;
wh = 2*pi*fh/Fs;
h = zeros(1,M+1);
for i = -M:1:M
	if i == 0
		h(i+1+M) = 1-(wh-wd)/pi;
	else
		h(i+1+M) = (sin(wd*i)-sin(wh*i))/(pi*i);
	end
end
end