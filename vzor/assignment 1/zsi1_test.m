close all;
clear;
clc;

%% Convolution
fprintf('--- Convolution test ---\n');
S = rand(1, 2^17);
h = rand(1, 100);

tic; c1 = conv(S, h); fprintf('conv() - %.2f s.\n', toc);
tic; c2 = myconv(S, h); fprintf('myconv() - %.2f s.\n', toc);

if c1 == c2
	fprintf('Both functions return the same result\n');
else
	fprintf(2, 'Functions does not return same result!\n');
	disp(c1(1:5));
	disp(c2(1:5));
end

%% Filter
fprintf('\n--- Filter test ---\n');
a = rand(1, 200);
b = rand(1, 200);

tic; f1 = filter(b, a, S); fprintf('filter() - %.2f s.\n', toc);
tic; f2 = myfilter(b, a, S); fprintf('myfilter() - %.2f s.\n', toc);

ok = 1;
for k = 1:min(find(isnan(f1-f2)))-100 %#ok<MXFND>
	if abs(f1(k)-f2(k)) > 1e7*eps(min(abs(f1(k+1)),abs(f2(k+1))))
		ok = 0;
		break;
	end
end
if ok
	fprintf('Both functions return the same result\n');
else
	fprintf(2, 'Functions does not return same result!\n');
	disp(f1(1:5));
	disp(f2(1:5));
end
clear k ok

%% Fast Fourier Transform
fprintf('\n--- Fast Fourier Transform test ---\n');

tic; X1 = fft(S); fprintf('fft() - %.2f s.\n', toc);
tic; X2 = myfft(S); X2=X2(end:-1:1); fprintf('myfft() - %.2f s.\n', toc);

if abs(X1-X2) < 1e7*eps(min(abs(X1),abs(X2)))
	fprintf('Both functions return the same result\n');
else
	fprintf(2, 'Functions does not return same result!\n');
	disp(X1(1:8));
	disp(X2(1:8));
end
