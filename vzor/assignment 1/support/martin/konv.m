function [ y ] = konv(b,a,signal)
%KONV Summary of this function goes here
%   Detailed explanation goes here
M=length(b);
N=length(a);
b=b/a(1);
a=a/a(1);
y=zeros(1,length(signal));
if length(a)==1 && a==1
    signal=[signal zeros(1,M-1)];
    for i=1:length(signal)
        p1=0;
        if i<M
            for j=1:i
                p1=p1+b(j)*signal(i-j+1);
            end
        else
            for j=1:M
                p1=p1+b(j)*signal(i-j+1);
            end
        end
        y(i)=p1;
    end
else
for i=1:length(signal)
    p1=0;
    if i<M
        for j=1:i
            p1=p1+b(j)*signal(i-j+1);
        end
    else
        for j=1:M
            p1=p1+b(j)*signal(i-j+1);
        end
    end
    
    p2=0;
    if i<N
        for k=1:i
            p2=p2-a(k)*y(i-k+1);
        end
    else
        for k=1:N
            p2=p2-a(k)*y(i-k+1);
        end
    end
    y(i)=p1+p2;
end
end
end

