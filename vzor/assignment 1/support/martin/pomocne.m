clc
close all
clear

[signal,fvz]=wavread('veta.wav');

pvz=65000;
delka=length(signal);
segment=2^10;
kroku= delka/segment;
signal(delka:ceil(kroku)*segment)=0;
delka=length(signal);
kroku= delka/segment;

for i=1:1:floor(pvz/segment)
    k0=(i-1)*segment+1;
    k1=i*segment;
    summ(i,:)=abs(fft(signal(k0:k1)));
    pom=2*abs(fft(signal(k0:k1))/length(signal(k0:k1)));
    su(i,:)=(pom(1:length(pom)/2));
end
summ=mean(summ)';
su=mean(su)';

odecteno=[];
alfa=1;
for krok=1:1:kroku
    k0=(krok-1)*segment+1;
    k1=krok*segment;
    vst=fft(signal(k0:k1));
    faze=angle(vst);
    amp=abs(vst);
    pom=[];
    for pp=1:1:length(summ)
        if amp(pp)-summ(pp)*alfa<0
            pom(pp)=0;
        else
            pom(pp)=amp(pp)-summ(pp)*alfa;
        end
    end
    sekv=pom'.*exp(1i*faze);
    odecteno(k0:k1,1)=real(ifft(sekv));
end

figure;
S=2*abs(fft(signal)/length(signal));
Z=(S(1:length(S)/2));
plot(linspace(0,(fvz/2)/1000,length(Z)),Z)
title('Spektrum sign�lu p�ed spektr�ln�m od��t�n�m')
xlabel('Frekvence [kHz]')
ylabel('Amplituda [-]')

wavplay(signal,fvz);

figure;
S=2*abs(fft(odecteno)/length(odecteno));
Z=(S(1:length(S)/2));
plot(linspace(0,(fvz/2)/1000,length(Z)),Z)
title('Spektrum sign�lu po spektr�ln�m od��t�n�')
xlabel('Frekvence [kHz]')
ylabel('Amplituda [-]')

wavplay(odecteno,fvz);

figure;
plot(linspace(0,(fvz/2)/1000,length(su)),su)
title('Spektrum sign�lu �umu')
xlabel('Frekvence [kHz]')
ylabel('Amplituda [-]')