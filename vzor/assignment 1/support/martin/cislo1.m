clc;close all;clear;
[signal,fvz]=wavread('veta.wav');

f=6000;
okoli=500;

%% FIR pasmova zadrz
NF=200;
fd=f-okoli;
fh=f+okoli;
% fh=fvz/2;
wd=(2*pi*fd)/fvz;
wh=(2*pi*fh)/fvz;
koef=[];
for k=-NF:1:NF
    if k == 0
        koef=[koef 1-(wh-wd)/pi];
    else
        koef=[koef (sin(wd*k)-sin(wh*k))/(pi*k)];
    end
end

hammi=[];
hanni=[];
black=[];
for k=0:2*NF
    hammi=[hammi (0.54-0.46*cos(2*pi*k/(2*NF)))];
    hanni=[hanni 0.5*(1-cos(2*pi*k/(2*NF)))];
    black=[black (0.42-0.5*cos(2*pi*k/(2*NF))+0.08*cos(4*pi*k/(2*NF)))];
end

filtr=hammi.*koef;
figure
freqz(filtr)
title(['FIR BS n=',num2str(NF)])

pause;
%% FIR dolni propust
NF=200;
fd=4000;
fh=fvz/2;
wd=(2*pi*fd)/fvz;
wh=(2*pi*fh)/fvz;
koef=[];
for k=-NF:1:NF
    if k == 0
        koef=[koef 1-(wh-wd)/pi];
    else
        koef=[koef (sin(wd*k)-sin(wh*k))/(pi*k)];
    end
end

orez=hammi.*koef;
figure
freqz(orez)
title(['FIR LP n=',num2str(NF)])

doplnek=(2^ceil(log2(length(signal)))-length(signal));
signal=[signal;zeros(doplnek,1)];
df=fvz/length(signal);

zvuk=signal;
delka=length(zvuk);
S=2*abs(fft(zvuk)/delka);
V=(S(1:length(S)/2));
figure
plot(df/1000:df/1000:(fvz/2)/1000,V)
title('Spektrum p�vodn�ho sign�lu')
xlabel('Frekvence [kHz]')
ylabel('Amplituda [-]')
pause;
sound(zvuk,fvz);
zvuk=zvuk';
for p = 1:2
zvuk=konv(filtr,[1],zvuk);
end

pause;
sound(zvuk,fvz);
S=2*abs(fft(zvuk(1:delka))/delka);
Z=(S(1:length(S)/2));
figure
plot(df/1000:df/1000:(fvz/2)/1000,Z)
title(['Spektrum odfiltrovan�ho sign�lu - FIR BS n=',num2str(NF)])
xlabel('Frekvence [kHz]')
ylabel('Amplituda [-]')

wavwrite(zvuk,fvz,'fir_bs.wav')

for p = 1:2
zvuk=konv(orez,[1],zvuk);
end
S=2*abs(fft(zvuk(1:delka))/delka);
R=(S(1:length(S)/2));
figure
plot(df/1000:df/1000:(fvz/2)/1000,R)
title(['Spektrum o��znut�ho sign�lu - FIR LP n=',num2str(NF)])
xlabel('Frekvence [kHz]')
ylabel('Amplituda [-]')

wavwrite(zvuk,fvz,'fir_lp.wav')
%% IIR doln� propust

Wp=3000/(0.5*fvz);
Ws=4000/(0.5*fvz);
Rp=1;
Rs=40;
[n,Wn] = buttord(Wp,Ws,Rp,Rs);
[b,a] = butter(n,Wn);
figure
freqz(b,a);
title(['IIR Butterworth LowPass n=',num2str(n)])

zvuk=signal;
zvuk=konv(b,a,zvuk);
S=2*abs(fft(zvuk(1:delka))/delka);
Z=(S(1:length(S)/2));
figure
plot(df/1000:df/1000:(fvz/2)/1000,Z)
title(['Spektrum filtrovan�ho sign�lu - IIR LP n=',num2str(n)])
xlabel('Frekvence [kHz]')
ylabel('Amplituda [-]')

wavwrite(zvuk,fvz,'iir_lp.wav')


%% IIR  pasmova zadrz
o=50;

Wp=[f-o f+o]/(0.5*fvz);
Ws=[f-1 f+1]/(0.5*fvz);
Rp=1;
Rs=40;

[n,Wn]=buttord(Wp,Ws,Rp,Rs);
[b,a]=butter(n,Wn,'stop');
figure
freqz(b,a); 
title(['IIR Butterworth BandStop n=',num2str(n)])

zvuk=signal;
zvuk=konv(b,a,zvuk);
S=2*abs(fft(zvuk(1:delka))/delka);
Z=(S(1:length(S)/2));
figure
plot(df/1000:df/1000:(fvz/2)/1000,Z)
title(['Spektrum filtrovan�ho sign�lu - IIR BS n=',num2str(n)])
xlabel('Frekvence [kHz]')
ylabel('Amplituda [-]')
wavwrite(zvuk,fvz,'iir_bs.wav')

prevzorkovano=prevzorkuj(zvuk,fvz,8000);
% wavplay(prevzorkovano,8000);
wavwrite(prevzorkovano,8000,'prevzorkovano.wav')

%% spektralni rozdil
mikrosegment=2^11;
pvz=13000;  %delka sumoveho signalu ve vzorcich
fvz=8000;

doplnek=(2^ceil(log2(length(prevzorkovano)))-length(prevzorkovano));
prevzorkovano=[prevzorkovano;zeros(doplnek,1)];

figure;
plot(1/fvz:1/fvz:1/fvz*length(prevzorkovano),prevzorkovano)
title('Sign�l po spektr�ln�m od��t�n�')
xlabel('�as [s]')
ylabel('Amplituda [-]')

puvodni=prevzorkovano(1:(floor(length(prevzorkovano)/mikrosegment)*mikrosegment));
kroku= length(puvodni)/mikrosegment;
summ=[];
su=[];
for i=1:1:floor(pvz/mikrosegment)
    k0=(i-1)*mikrosegment+1;
    k1=i*mikrosegment;
    summ(i,:)=abs(fft(puvodni(k0:k1)));
    pom=2*abs(fft(puvodni(k0:k1))/length(puvodni(k0:k1)));
    su(i,:)=(pom(1:length(pom)/2));
end
summ=mean(summ)';
su=mean(su)';

odecteno=[];
alfa=1;
for krok=1:1:kroku
    k0=(krok-1)*mikrosegment+1;
    k1=krok*mikrosegment;
    vst=fft(puvodni(k0:k1));
    faze=angle(vst);
    amp=abs(vst);
    pom=[];
    for pp=1:1:length(summ)
        if amp(pp)-summ(pp)*alfa<0
            pom(pp)=0;
        else
            pom(pp)=amp(pp)-summ(pp)*alfa;
        end
    end
    sekv=pom'.*exp(1i*faze);
    odecteno(k0:k1,1)=real(ifft(sekv));
end

wavwrite(odecteno,fvz,'spektralni_rozdil.wav')

df=fvz/length(prevzorkovano);

figure;
S=2*abs(fft(prevzorkovano)/length(prevzorkovano));
P=(S(1:length(S)/2));
plot(df/1000:df/1000:(fvz/2)/1000,P)
title('Spektrum sign�lu p�ed spektr�ln�m od��t�n�')
xlabel('Frekvence [kHz]')
ylabel('Amplituda [-]')

figure;
S=2*abs(fft(odecteno)/length(odecteno));
Z=(S(1:length(S)/2));
plot(df/1000:df/1000:(fvz/2)/1000,Z)
title('Spektrum sign�lu po spektr�ln�m od��t�n�')
xlabel('Frekvence [kHz]')
ylabel('Amplituda [-]')

figure;
plot(linspace(0,(fvz/2)/1000,length(su)),su)
title('Spektrum sign�lu �umu')
xlabel('Frekvence [kHz]')
ylabel('Amplituda [-]')


figure;
plot(1/fvz:1/fvz:1/fvz*length(odecteno),odecteno)
title('Sign�l po spektr�ln�m od��t�n�')
xlabel('�as [s]')
ylabel('Amplituda [-]')

T=2;
pr=prumer(puvodni,T);
figure;
S=2*abs(fft(pr)/length(pr));
Z=(S(1:length(S)/2));
plot(df/1000:df/1000:(fvz/2)/1000,Z)
title(['Spektrum sign�lu po pr�m�rov�n� p�es ',num2str(T),' vzork�'])
xlabel('Frekvence [kHz]')
ylabel('Amplituda [-]')
% wavplay(pr,8000);
wavwrite(pr,fvz,'prumerovani_2.wav')

T=5;
pr=prumer(puvodni,T);
figure;
S=2*abs(fft(pr)/length(pr));
Z=(S(1:length(S)/2));
plot(df/1000:df/1000:(fvz/2)/1000,Z)
title(['Spektrum sign�lu po pr�m�rov�n� p�es ',num2str(T),' vzork�'])
xlabel('Frekvence [kHz]')
ylabel('Amplituda [-]')
% wavplay(pr,8000);
wavwrite(pr,fvz,'prumerovani_5.wav')

T=10;
pr=prumer(puvodni,T);
figure;
S=2*abs(fft(pr)/length(pr));
Z=(S(1:length(S)/2));
plot(df/1000:df/1000:(fvz/2)/1000,Z)
title(['Spektrum sign�lu po pr�m�rov�n� p�es ',num2str(T),' vzork�'])
xlabel('Frekvence [kHz]')
ylabel('Amplituda [-]')
% wavplay(pr,8000);
wavwrite(pr,fvz,'prumerovani_10.wav')

% wavplay(puvodni,8000);
% wavplay(odecteno,8000);