function [ X ] = mfft( signal )
%MFFT Summary of this function goes here
%   Zpracovani ve frekvencni oblasti

N=length(signal);
% signal
for k=1:1:N/2
   A=k;B=k+N/2; 
   X(k)=signal(A)+signal(B); 
end

for k=N/2+1:1:N
    W=k-1-N/2;
    A=k-N/2;B=k;
    X(k)=(signal(A)-signal(B))*exp(-j*2*pi/N*W);
end
if N>2    
    G = mfft(X(1:1:N/2));
    H = mfft(X(N/2+1:1:N));    
    X = [];
    %X=[mfft(X(1:1:N/2)) mfft(X(N/2+1:1:N))];
  
    for k=1:1:N/2
        X=[X G(k) H(k)];
    end;    
end
end