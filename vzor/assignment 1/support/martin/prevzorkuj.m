function [ output ] = prevzorkuj( data, fs_in, fs_out )
%PREVZORKUJ Summary of this function goes here
%   Detailed explanation goes here

d=length(data);

K=fs_in/fs_out;
[e,~]=size(data);
x=[];

for k=1:K:d
    k0=floor(k);
    k1=ceil(k);
    if k==k0
        x=[x;data(k)];
    else
        x=[x;(data(k0)+data(k1))/(k0+k1)*k];
    end
            
end
output=x;
end

