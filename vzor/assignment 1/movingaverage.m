function Sf = movingaverage(S, samples)

Sf = zeros(1, length(S)-samples+1);
for i = 1:length(S)-samples+1
	Sf(i) = mean(S(i:i+samples-1));
end
end
