function [y] = myconv(S, h)

if length(h) > length(S)
	a = h;
	h = S;
	S = a;
end

m = length(S);
n = length(h);
S = [S, zeros(1, n)];
y = zeros(1, m+n-1);

for k = 1:m+n-1
	for l = 1:n
		if(k-l+1 > 0)
			y(k) = y(k) + h(l) * S(k-l+1);
		else
			break;
		end
	end
end
end