function [wHN, wHM, wBL] = getWindows(M)

wHN = zeros(1,M+1);
wHM = zeros(1,M+1);
wBL = zeros(1,M+1);
for i = 0:2*M
	wHN(i+1) = 0.5 * (1 - cos(2*pi*i / (2*M)));
	wHM(i+1) = 0.54 - 0.46 * cos(2*pi*i / (2*M));
	wBL(i+1) = 0.42 - 0.5 * cos(2*pi*i / (2*M)) + 0.08 * cos(4*pi*i / (2*M));
end
% wHN = zeros(1,M+1);
% wHM = zeros(1,M+1);
% wBL = zeros(1,M+1);
% for i = 0:M
%     wHN(i+1) = 0.5 * (1 - cos(2*pi*i / M));
%     wHM(i+1) = 0.54 - 0.46 * cos(2*pi*i / M);
%     wBL(i+1) = 0.42 - 0.5 * cos(2*pi*i / M) + 0.08 * cos(4*pi*i / M);
% end
end