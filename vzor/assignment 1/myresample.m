function Z = myresample(S, Fs, newFs)

K = Fs / newFs;
Z = zeros(1, Fs/K);
ind = 1;
for k = 1:K:length(S)
	kf = floor(k);
	kc = ceil(k);
	if k == kf
		Z(ind) = S(k);
	else
		Z(ind) = S(kf) + (S(kc)-S(kf))*(k-kf);
	end
	ind = ind+1;
end
end