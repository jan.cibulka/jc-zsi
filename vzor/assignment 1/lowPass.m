function [h] = lowPass(f, Fs, M)

wd = 2*pi*f/Fs;
h = zeros(1,M+1);
for i = -M:1:M
	if i == 0
		h(i+1+M) = wd/pi;
	else
		h(i+1+M) = sin(wd*i)/(pi*i);
	end
end
end