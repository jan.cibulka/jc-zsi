close all;
clear;
clc;
tic;

%%	Load signal and prepare useful variables
filename = 'veta46.wav';
[S, Fs] = audioread(filename);
S = S';
supple = 2^ceil(log2(length(S))) - length(S);
S = [S, zeros(1, supple)];

L = length(S);			% Length of signal
dF = Fs/L;
f = 0:dF:Fs/2-dF;		% Frequency range
fprintf('Signal loaded. (%.2f s)\n', toc);

%%	Find parasite frequency

X = fft(S);
figure; plot(f, abs(X(1:L/2))/L);
set(gcf, 'Position', [353, 216, 740, 420]);
xlim([0, 1.2e4]);
xlabel('Frequency [Hz]');
ylabel('Magnitude');
title('Signal frequency spectrum');
fprintf('Signal frequency spectrum. (%.2f s)\n', toc);

%%	FIR filters
M = 200;
[wHN, wHM, wBL] = getWindows(M);

% Get filters coefficients
lowCoef = lowPass(5750, Fs, M);
fprintf('FIR Lowpass filter. (%.2f s)\n', toc);
bandCoef = bandStop(5800, 6200, Fs, M);
fprintf('FIR Bandstop filter. (%.2f s)\n', toc);

% Compare windows
lpHN = wHN.*lowCoef;
lpHM = wHM.*lowCoef;
lpBL = wBL.*lowCoef;
figure;
freqz(lpHN); hold on; freqz(lpHM); freqz(lpBL);
lines = findall(gcf,'type','line');
lines(2).Color = 'red'; lines(3).Color = 'green';
set(gcf, 'Position', [353, 216, 740, 420]);
title('FIR Lowpass filter windows comparsion');
legend('Hann', 'Hamming', 'Black', 'Location', 'SouthWest'); hold off;

bsHN = wHN.*bandCoef;
bsHM = wHM.*bandCoef;
bsBL = wBL.*bandCoef;
figure;
freqz(bsHN); hold on; freqz(bsHM); freqz(bsBL);
lines = findall(gcf,'type','line');
lines(2).Color = 'red'; lines(3).Color = 'green';
set(gcf, 'Position', [353, 216, 740, 420]);
title('FIR Bandstop filter windows comparsion');
legend('Hann', 'Hamming', 'Black', 'Location', 'SouthEast'); hold off;

% Use filters with hamming window
SlowFIR = conv(S, lpHM);
SbandFIR = conv(S, bsHM);
fprintf('FIR filtration. (%.2f s)\n', toc);

% Plot frequency spectrum
XlowFIR = fft(SlowFIR);
XbandFIR = fft(SbandFIR);

figure;
plot(f, abs(X(1:L/2))/L); hold on;
plot(f, abs(XbandFIR(1:L/2))/L);
plot(f, abs(XlowFIR(1:L/2))/L);
set(gcf, 'Position', [353, 216, 740, 420]);
xlabel('Frequency [Hz]'); ylabel('Magnitude');
title('Signal frequency spectrum (FIR)');
xlim([0, 1.2e4]); ylim([0, 14e-4]);
legend('Origin', 'Bandstop', 'Lowpass'); hold off;

% Play samples
% sound(S(1:185000), Fs);
% pause(5)
% sound(SlowFIR(1:185000), Fs);
% pause(5)
% sound(SbandFIR(1:185000), Fs);
% pause(5)
audiowrite('11_fir_lowpass.wav', SlowFIR, Fs);
audiowrite('12_fir_bandstop.wav', SbandFIR, Fs);

%%	IIR filters
% Lowpass
Wp = 5000/(0.5*Fs);
Ws = 5770/(0.5*Fs);
Rp = 1;
Rs = 45;
[n, Wn] = buttord(Wp, Ws, Rp, Rs);
[lowB, lowA] = butter(n, Wn);
figure; freqz(lowB, lowA);
set(gcf, 'Position', [353, 216, 740, 420]);
title('IIR Butterworth Lowpass filter');
fprintf('IIR Lowpass filter. (%.2f s)\n', toc);
fprintf(2, 'IIR BW LP order: %d\n', n);

% Bandstop
Wp = [5950, 6050]/(0.5*Fs);
Ws = [5999, 6001]/(0.5*Fs);
Rp = 1;
Rs = 40;
[n, Wn] = buttord(Wp, Ws, Rp, Rs);
[bandB, bandA] = butter(n, Wn, 'stop');
figure; freqz(bandB, bandA);
set(gcf, 'Position', [353, 216, 740, 420]);
title('IIR Butterworth Bandstop filter');
fprintf('IIR Bandstop filter. (%.2f s)\n', toc);
fprintf(2, 'IIR BW BS order: %d\n', n);

SlowIIR = filter(lowB, lowA, S);
SbandIIR = filter(bandB, bandA, S);

% Plot frequency spectrum
XlowIIR = fft(SlowIIR);
XbandIIR = fft(SbandIIR);
fprintf('IIR filtration. (%.2f s)\n', toc);

figure;
plot(f, abs(X(1:L/2))/L); hold on;
plot(f, abs(XbandIIR(1:L/2))/L);
plot(f, abs(XlowIIR(1:L/2))/L);
set(gcf, 'Position', [353, 216, 740, 420]);
xlabel('Frequency [Hz]'); ylabel('Magnitude');
title('Signal frequency spectrum (IIR)');
xlim([0, 1.2e4]); ylim([0, 14e-4]);
legend('Origin', 'Bandstop', 'Lowpass'); hold off;

% Play samples
% sound(S(1:185000), Fs);
% pause(5)
% sound(SlowIIR(1:185000), Fs);
% pause(5)
% sound(SbandIIR(1:185000), Fs);
% pause(5)
audiowrite('13_iir_lowpass.wav', SlowIIR, Fs);
audiowrite('14_iir_bandstop.wav', SbandIIR, Fs);

%%	Resample
Fs8 = 8000;

S4k = conv(S, wHM .* lowPass(4000, Fs, M));
X4k = fft(S4k);

figure;
plot(f, abs(X4k(1:L/2))/L);
set(gcf, 'Position', [353, 216, 740, 420]);
xlabel('Frequency [Hz]'); ylabel('Magnitude');
title('Frequency spectrum of signal for resampling');
xlim([0, 1.2e4]); ylim([0, 14e-4]);

S8 = myresample(S4k, Fs, Fs8);
supple = 2^ceil(log2(length(S8))) - length(S8);
S8 = [S8, zeros(1, supple)];

L8 = size(S8, 2);
dF8 = Fs8/L8;
f8 = 0:dF8:Fs8/2-dF8;
fprintf('Signal resample. (%.2f s)\n', toc);

% Play samples
% sound(S4k(1:185000), Fs);
% pause(5)
% sound(S8(1:34000), Fs2);
% pause(5)
audiowrite('2_resampled.wav', S8, Fs8);

close all;
%%	Remove noise
% Frequency subtraction
N = [S8(1:8374), S8(32123:33272), S8(65440:72299)];
LN = length(N);
piece = 2^10;

Xns = zeros(LN/piece, piece);
for k = 1:LN/piece
	Xns(k,:) = abs(fft(N((k-1)*piece+1 : (k-1)*piece+piece)));
end
Xn = mean(Xns);
dFn = Fs8/piece;
fn = 0:dFn:Fs8/2-dFn;

S8f = zeros(1, length(S8));
for k = 1:L8/piece
	a = (k-1)*piece+1;
	b = (k-1)*piece+piece;
	x = fft(S8(a:b));
	pha = angle(x);
	amp = abs(x) - Xn;
	amp(amp<0) = 0;
	
	S8f(a:b) = real(ifft(amp.*exp(1i*pha)));
end
fprintf('Frequency noise removal. (%.2f s)\n', toc);

X8 = fft(S8);
X8f = fft(S8f);

figure;
plot(f8, abs(X8(1:L8/2))/L8); hold on;
plot(f8, abs(X8f(1:L8/2))/L8);
set(gcf, 'Position', [353, 216, 740, 420]);
xlabel('Frequency [Hz]'); ylabel('Magnitude');
title('Spectrum of frequency noise removal');
legend('Origin', 'Without noise'); hold off;

% Play samples
% sound(S8(1:34000), Fs8);
% pause(5)
% sound(S8f(1:34000), Fs8);
% pause(5)
audiowrite('31_frequency_sub.wav', S8f, Fs8);

% Moving average
S82 = movingaverage(S8, 2);
fprintf('Moving average (2) noise removal. (%.2f s)\n', toc);
S85 = movingaverage(S8, 5);
fprintf('Moving average (5) noise removal. (%.2f s)\n', toc);
S810 = movingaverage(S8, 10);
fprintf('Moving average (10) noise removal. (%.2f s)\n', toc);

X82 = fft(S82);
X85 = fft(S85);
X810 = fft(S810);

figure;
plot(f8, abs(X8(1:L8/2))/L8); hold on;
plot(f8(1:end-1), abs(X82(1:floor(length(S82)/2)))/length(S82));
plot(f8(1:end-2), abs(X85(1:floor(length(S85)/2)))/length(S85));
plot(f8(1:end-5), abs(X810(1:floor(length(S810)/2)))/length(X810));
set(gcf, 'Position', [353, 216, 740, 420]);
xlabel('Frequency [Hz]'); ylabel('Magnitude');
title('Signal frequency spectrum after moving average');
legend('Origin', '2 samples', '5 samples', '10 samples'); hold off;

% Play samples
% sound(S8(1:34000), Fs8);
% pause(5)
% sound(S82(1:34000), Fs8);
% pause(5)
% sound(S85(1:34000), Fs8);
% pause(5)
% sound(S810(1:34000), Fs8);
% pause(5)
audiowrite('32_average_2.wav', S82, Fs8);
audiowrite('33_average_5.wav', S85, Fs8);
audiowrite('34_average_10.wav', S810, Fs8);


