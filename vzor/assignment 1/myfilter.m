function y = myfilter(b, a, S)

b=b/a(1);
a=a/a(1);
m = length(S);
n = length(b);
S = [S, zeros(1, n)];
y = zeros(1, n+m-1);

for k = 1:m+n-1
	val = 0;
	for l = 1:n
		if(k-l+1 > 0)
			val = val + b(l) * S(k-l+1);
			val = val - a(l) * y(k-l+1);
		end
	end
% 	for l = 1:n
% 		if(k-l+1 > 0)
% % 			val = val + b(l) * S(k-l+1);
% 			val = val - a(l) * y(k-l+1);
% 		end
% 	end
	y(k) = val;
end
y = y(1:m);
end