close all;
clear;
clc;

%%	1.�Na�t�te�sign�l�ze�souboru�do�Matlabu�a�zobrazte.
filename = 'veta46.wav';
[S, Fs] = audioread(filename);
S = S';
% S = S(1:10000);

T = 1/Fs;				% Sampling period
L = size(S, 2);			% Length of signal
t = (0:L-1) * T;		% Time vector

plot(Fs * t, S);
title('Voice signal');
xlabel('Signal samples');
ylabel('Amplitude');

%%	2.�Navrn�te�a�implementujte�funkci�pro�identifikaci�lok�ln�ch�extr�m��sign�lu
%	a�jeho�pr�chod� nulou

[maxs, mins, zers] = getExtremes(S);

%%	3.�S�vyu�it�m�funkce�z�bodu�2�implementujte�empirickou�mod�ln��dekompozici
%	s�omezuj�c�m krit�riem,�kter�m�bude�p�edem�zvolen� maxim�ln��po�et iterac�
%	nalezen��IMF.

iterations = 9;

IMF = EMD(S, iterations, 50);
figure;	surf(IMF,'MeshStyle','row');
title('IMF components');
xlabel('Signal samples');
ylabel('IMFs');
zlabel('Amplitude');
view([1 1 1]);

%%  4.�Jak�se�li���frekven�n��p�sma�jednotliv�ch�IMF?
%	Porovnejte�amplitudov�spektra�jednotliv�ch IMF,�kter�jsou�v�sledkem�z�bodu�3.

figure; hold on;
leg = cell(iterations+1,1);
leg{1} = 'Base signal';
% maxFreq = 60000;
for i = 1:iterations+1
	f = fft(IMF(i,:))/(length(IMF(i,:))/2);
	a = abs(f(1:length(f)/2));
	plot(1:length(f)/2, a)
% 	plot(1:maxFreq, abs(f(1:maxFreq)))
	if i > 1; leg{i} = sprintf('IMF %d', i-1); end
end
title('Spectrum of indiviidual components');
xlabel('Frequency [Hz]');
ylabel('Amplitude');
legend(leg);
hold off;

%%	5.�Vyhodno�te�okam�itou�frekvenci�jednotliv�ch�IMF.
%	Jak��vliv�m�apriorn��volba�hodnoty kriteria�(maxim�ln��po�et�iterac�)�na�kvalitu
%	okam�it�frekvence�slo�ek�sign�lu?�

hilb = zeros(iterations+1, length(S)-1);
h_var = zeros(iterations+1, 1);
for i = 1:iterations+1
	hilb(i,:) = diff(unwrap(angle(hilbert(IMF(i,:)))));
	h_var(i,:) = var(hilb(i,:));
end

figure; surf(hilb, 'MeshStyle', 'row');
title('Instantaneous frequency of individual components')
xlabel('Signal samples')
ylabel('Components')
zlabel('Frequency')
view([1 1 1])
