close all;
clear;
clc;

%%	1.�Na�t�te�sign�l�ze�souboru�do�Matlabu�a�zobrazte.
filename = 'veta46.wav';
[S, Fs] = audioread(filename);
S = S';
S = S(1:1000);

T = 1/Fs;             % Sampling period
L = size(S, 2);             % Length of signal
t = (0:L-1) * T;        % Time vector

% plot(Fs * t, S)
% % plot(Fs * t(1:100), S(1:100))
% title('Voice signal')
% xlabel('t (milliseconds)')
% ylabel('X(t)')

%%	2.�Navrn�te�a�implementujte�funkci�pro�identifikaci�lok�ln�ch�extr�m��sign�lu
%	a�jeho�pr�chod� nulou

[maxs, mins, zers] = getExtremes(S);

%%	3.�S�vyu�it�m�funkce�z�bodu�2�implementujte�empirickou�mod�ln��dekompozici
%	s�omezuj�c�m krit�riem,�kter�m�bude�p�edem�zvolen�
%	maxim�ln��po�et�iterac��nalezen��IMF.

signal = EMD(S, Fs, 1000);
figure; hold on;
plot(Fs*t, S)
for i = 1:size(signal,1)
	if i == 5 || i == 10 || i == 50 || i == 500 || i == 1000
		plot(Fs*t, signal(i,:))
	end
end
legend 0 5 10 50 500 1000;



% Fs = 1000;            % Sampling frequency
% T = 1/Fs;             % Sampling period
% L = size(S,2);             % Length of signal
% t = (0:L-1)*T;        % Time vector

% S = 0.7*sin(2*pi*50*t) + sin(2*pi*120*t);

% X = S + 2*randn(size(t));

% size(t)
% size(X)
% pause;
plot(Fs*t(1:500),X(1:500))
title('Voice signal')
xlabel('t (milliseconds)')
ylabel('X(t)')

Y = fft(X);

P2 = abs(Y/L);
P1 = P2(1:L/2+1);
P1(2:end-1) = 2*P1(2:end-1);

f = Fs*(0:(L/2))/L;
figure; plot(f,P1);
title('Single-Sided Amplitude Spectrum of X(t)')
xlabel('f (Hz)')
ylabel('|P1(f)|')

Y = fft(S);
P2 = abs(Y/L);
P1 = P2(1:L/2+1);
P1(2:end-1) = 2*P1(2:end-1);

figure; plot(f,P1);
title('Single-Sided Amplitude Spectrum of S(t)')
xlabel('f (Hz)')
ylabel('|P1(f)|')