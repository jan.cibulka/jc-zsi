clc; close all; clear;
[signal,fvz]=wavread('veta.wav');
signal=signal(1:500);
signal=signal';
vstup=signal;

data=signal;
krok=1:length(data);
[minima,nuly,maxima]=lext(data);
        mi=spline(minima,signal(minima),krok);
        ma=spline(maxima,signal(maxima),krok);
        s=(mi+ma)/2;
        data=data-s;

hold on
plot(mi,'g')
plot(ma,'r')
plot(s,'k')
plot(signal)
hold off
title('Jeden krok EMD')
xlabel('Vzorky')
ylabel('Amplituda [-]')
legend('Lok�ln� minima','Lok�ln� maxima','S�edn� hodnota','Vstupn� sign�l')