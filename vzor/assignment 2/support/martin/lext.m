function [ minima,nuly,maxima ] = lext( signal )
%LEXT Summary of this function goes here
%   Detailed explanation goes here
nuly=[];
minima=[1];
maxima=[1];


for i=2:length(signal)-1
    if signal(i-1)<signal(i) && signal(i)>=signal(i+1)
        maxima=[maxima i];
    end
    if signal(i-1)>signal(i) && signal(i)<=signal(i+1)
        minima=[minima i];
    end
    if signal(i)==0
        nuly=[nuly i];
    end
    if signal(i)<0 && signal(i-1)>0
        nuly=[nuly i];
    end
    if signal(i)>0 && signal(i-1)<0
        nuly=[nuly i];
    end 
end
maxima=[maxima length(signal)];
minima=[minima length(signal)];
end

