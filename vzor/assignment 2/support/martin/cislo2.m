close all;
clc; clear;

% load('IMF.mat')
% load('fre.mat')
% load('fimf')

fvz=8000;
load('veta.mat')
signal=prevzorkovano(1:94521)';
% [signal,fvz]=wavread('veta.wav');
iteraci=9;

figure
% signal=signal';
plot(1/fvz:1/fvz:1/fvz*length(signal),signal);
vstup=signal;

vyhodnoceni=[];
for opak=1:20,000281144906630947	2,01064445615928e-07	0,000293125376145775	2,04858933798321e-07
0,000172324178412265	1,46851171503782e-07	0,000186455776396149	1,58399904023277e-07
9,32783371609048e-05	7,80698086432582e-08	0,000107594356708681	9,27273826439692e-08
4,55064327386323e-05	2,92723182625166e-08	5,67568377665130e-05	4,09362996916798e-08
2,20269993884626e-05	1,04605320724624e-08	2,86868845614931e-05	1,52953396962001e-08
1,14117708015086e-05	3,96760585121788e-09	1,48441438021128e-05	5,63361023865240e-09
7,51584304229811e-06	2,21318752235542e-09	8,89488969386641e-06	2,81214945955983e-09
5,69508848439858e-06	1,45928907620675e-09	6,49904539451993e-06	1,82878372228968e-09
5,49691866230177e-06	1,37460665837554e-09	5,41348744395946e-06	1,37503845084887e-09
signal=prevzorkovano(1:94521)';
IMF=[];
disp(['Aktuální stav ',num2str(0),'%'])
for j=1:iteraci
    data=signal;
    for i=1:1:50*opak
        krok=1:length(data);
        [minima,nuly,maxima]=lext(data);
        mi=spline(minima,data(minima),krok);
        ma=spline(maxima,data(maxima),krok);
        s=(mi+ma)/2;
        data=data-s;
    end
    signal=signal-data;
    podminka=sqrt(var(signal));
    IMF(j,:)=signal;
    disp(['Aktuální stav ',num2str(j/iteraci*100),'%'])
end

figure
surf(IMF,'MeshStyle','row')
title('IMF komponenty')
xlabel('Vzorky')
ylabel('Amplituda [-]')
view([1 1 1])

figure
hold on
delka=length(signal);
popis=[];
df=fvz/delka;
    
fre=[];
fimf=[];
for i=1:iteraci
    S=2*(fft(IMF(i,:))/delka);
    Z=(S(1:length(S)/2));
    fimf(i,:)=abs(Z);
    plot(df/1000:df/1000:(fvz/2)/1000,abs(Z),'Color',hsv2rgb([i/iteraci 1 1]))
    if i<10
        popis=[popis;['IMF 0',num2str(i)]];
    else
        popis=[popis;['IMF ',num2str(i)]];
    end
    fre(i,:)=diff(unwrap(angle(hilbert(IMF(i,:)))));
end
title('Spektrální analýza jednotlivých komponent')
xlabel('Frekvence [kHz]')
ylabel('Amplituda [-]')
legend(popis)
grid
hold off

figure
surf(fimf,'MeshStyle','row','FaceColor','none')
title('Zobrazení DFT jednotlivých komponent')
xlabel('Vzorky signálu')
ylabel('Komponenty')
zlabel('Amplituda [-]')
view([1 1 1])


figure
surf(fre,'MeshStyle','row')
title('Okamžité frekvence jednotlivých komponent')
xlabel('Vzorky signálu')
ylabel('Komponenty')
zlabel('Frekvence')
view([1 1 1])

vyhodnoceni(:,opak*2-1)=mean(fimf')';
vyhodnoceni(:,opak*2)=var(fimf')';
end
disp('Hotovo')