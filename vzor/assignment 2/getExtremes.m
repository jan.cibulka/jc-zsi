function [maxs, mins, zers] = getExtremes(S)
% getExtremes returns maxima, minima and zeros of signal
%	[maxs, mins, zers] = getExtremes(S)
%	S = [1,2,3,2,2]
%	maxs or mins = [indices]
%	zers = [indices]

% S = [1,2,3,3,2,3,4,3,2,0,0,1]

maxs = 1;
mins = 1;
zers = [];
for i = 2:length(S)-1
	if S(i-1)<0 && S(i)>=0 || S(i-1)>0 && S(i)<=0
		zers(end+1) = i; %#ok<AGROW>
	end
	if S(i-1)<S(i) && S(i)>=S(i+1)
		maxs(end+1) = i; %#ok<AGROW>
	elseif S(i-1)>S(i) && S(i)<=S(i+1)
		mins(end+1) = i; %#ok<AGROW>
	end
end
maxs(end+1) = length(S);
mins(end+1) = length(S);

% figure; hold on;
% plot(1:length(S), S);
% plot(maxs(1,:), S(maxs), 'o');
% plot(mins(1,:), S(mins), 'o');
% hold off;
end