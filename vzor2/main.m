%% ZSI - Signal Processing
% Semestral Work I
% Author: Bc. Jan Hranicka

close all;

%% Signal analysis
[y, Fs] = audioread('veta3_9.wav');
fs = 1/Fs;      % Sample period

t = linspace(0, numel(y)/Fs, numel(y));
plot(t, y);
title('Time course of input signal', 'Interpreter', 'latex');
xlabel('Time (s)', 'Interpreter', 'latex')
ylabel('Amplitude', 'Interpreter', 'latex');
xlim([0, numel(y)/Fs]);

figure;
subplot(2,1,1);
plot_spectrum(y, Fs);
subplot(2,1,2);
spectrogram(y,1048,[],[],Fs,'yaxis');

%% FIR - Low-pass filter
[y_out_lp, h_lp] = lpfilter(y, Fs, 10700, 400);
audiowrite('output_lp.wav', y_out_lp, Fs)

figure;
subplot(2,1,1);
spectrogram(y,1048,[],[],Fs,'yaxis');
title('Spectrogram of original signal', 'interpreter', 'latex');
xlabel('Time (s)', 'interpreter', 'latex');
ylabel('Frequency (kHz)', 'interpreter', 'latex');
subplot(2,1,2);
spectrogram(y_out_lp,1048,[],[],Fs,'yaxis');
title('Spectrogram of filtered signal with FIR low-pass filter', 'interpreter', 'latex');
xlabel('Time (s)', 'interpreter', 'latex');
ylabel('Frequency (kHz)', 'interpreter', 'latex');

figure;
plot_spectrum(y,Fs, 'Positive');
hold on;
plot_spectrum(y_out_lp, Fs, 'Positive');
legend('original signal', 'FIR low-pass filtered');

%% FIR - Band-stop filter
[y_out_bs, h_bs] = bsfilter(y, Fs, 10700, 11300, 1000);
audiowrite('output_bs.wav', y_out_bs, Fs)

figure;
subplot(2,1,1);
spectrogram(y,1048,[],[],Fs,'yaxis');
title('Spectrogram of original signal', 'interpreter', 'latex');
xlabel('Time (s)', 'interpreter', 'latex');
ylabel('Frequency (kHz)', 'interpreter', 'latex');
subplot(2,1,2);
spectrogram(y_out_bs,1048,[],[],Fs,'yaxis');
title('Spectrogram of filtered signal with FIR band-stop filter', 'interpreter', 'latex');
xlabel('Time (s)', 'interpreter', 'latex');
ylabel('Frequency (kHz)', 'interpreter', 'latex');

figure;
plot_spectrum(y, Fs, 'Positive');
hold on;
plot_spectrum(y_out_bs, Fs, 'Positive');
legend('original signal', 'FIR band-stop filtered');

figure;
plot_spectrum(y,Fs, 'Positive')
hold on;
plot_spectrum(y_out_lp,Fs, 'Positive')
plot_spectrum(y_out_bs,Fs, 'Positive')
legend('original signal','FIR low-pass filtered','FIR band-stop filtered');

fvtool(h_lp, 1);
fvtool(h_bs, 1);

%% FIR - filters with different kinds of window function
load('hann.mat');
load('hamming.mat');
load('blackman.mat');

y_hann = filter(Hann, y);
y_hamming = filter(Hamming, y);
y_blackman = filter(Blackman, y);

% Spectrograms
figure;
subplot(3,1,1);
spectrogram(y_hann,1048,[],[],Fs,'yaxis');
title('Spectrogram of filtered signal with FIR band-stop Hann filter', 'interpreter', 'latex');
xlabel('Time (s)', 'interpreter', 'latex');
ylabel('Frequency (kHz)', 'interpreter', 'latex');
subplot(3,1,2);
spectrogram(y_hamming,1048,[],[],Fs,'yaxis');
title('Spectrogram of filtered signal with FIR band-stop Hamming filter', 'interpreter', 'latex');
xlabel('Time (s)', 'interpreter', 'latex');
ylabel('Frequency (kHz)', 'interpreter', 'latex');
subplot(3,1,3);
spectrogram(y_blackman,1048,[],[],Fs,'yaxis');
title('Spectrogram of filtered signal with FIR band-stop filter', 'interpreter', 'latex');
xlabel('Time (s)', 'interpreter', 'latex');
ylabel('Frequency (kHz)', 'interpreter', 'latex');

% Spectrums
figure;
subplot(3,1,1);
plot_spectrum(y_hann, Fs);
subplot(3,1,2);
plot_spectrum(y_hamming, Fs);
subplot(3,1,3);
plot_spectrum(y_blackman, Fs);
% legend('Filtered with Hann window', 'Filtered with Hamming window', 'Filtered with Blackman window');


%% IIR filters design
close all;
load('butterworth.mat');
load('chebyshev.mat');
load('ichebyshev.mat');
load('elliptic.mat');

y_butterworth = filter(Butterworth, y);
y_chebyshev = filter(Chebyshev, y);
y_ichebyshev = filter(IChebyshev, y);
y_elliptic = filter(Elliptic, y);

% Spectrograms
figure;
subplot(4,1,1);
spectrogram(y_butterworth,1048,[],[],Fs,'yaxis');
title('Spectrogram of filtered signal with IIR band-stop Butterworth filter', 'interpreter', 'latex');
xlabel('Time (s)', 'interpreter', 'latex');
ylabel('Frequency (kHz)', 'interpreter', 'latex');

subplot(4,1,2);
spectrogram(y_chebyshev,1048,[],[],Fs,'yaxis');
title('Spectrogram of filtered signal with IIR band-stop Chebyshev filter', 'interpreter', 'latex');
xlabel('Time (s)', 'interpreter', 'latex');
ylabel('Frequency (kHz)', 'interpreter', 'latex');

subplot(4,1,3);
spectrogram(y_ichebyshev,1048,[],[],Fs,'yaxis');
title('Spectrogram of filtered signal with IIR band-stop Inverse Chebyshev filter', 'interpreter', 'latex');
xlabel('Time (s)', 'interpreter', 'latex');
ylabel('Frequency (kHz)', 'interpreter', 'latex');

subplot(4,1,4);
spectrogram(y_elliptic,1048,[],[],Fs,'yaxis');
title('Spectrogram of filtered signal with IIR band-stop Elliptic filter', 'interpreter', 'latex');
xlabel('Time (s)', 'interpreter', 'latex');
ylabel('Frequency (kHz)', 'interpreter', 'latex');

%% Resampling audio to 8 kHz
Fs8kHz = 8000;
L = 441;
M = 80;
y_down = resample(y_out_lp,L,M);

% Spectrums
figure;
plot_spectrum(y_out_lp,Fs);
hold on;
plot_spectrum(y_down, Fs8kHz);
legend('FIR low-pass filtered signal', 'resampled signal to 8 kHz');

% Spectrograms
figure;
subplot(2,1,1);
spectrogram(y_out_lp,1048,[],[],Fs,'yaxis');
title('Spectrogram of FIR low-pass filtered signal', 'interpreter', 'latex');
xlabel('Time (s)', 'interpreter', 'latex');
ylabel('Frequency (kHz)', 'interpreter', 'latex');
subplot(2,1,2);
spectrogram(y_down,1048,[],[],Fs8kHz,'yaxis');
title('Spectrogram of signal resampled to 8 kHz', 'interpreter', 'latex');
xlabel('Time (s)', 'interpreter', 'latex');
ylabel('Frequency (kHz)', 'interpreter', 'latex');

audiowrite('output_8khz.wav', y_down, Fs8kHz)

%% Noise/Speech detection
wlen = 256;
N = zeros(1, wlen);
rozsah = 22000;

y = [y_down; zeros(8, 1)];
wlen = 20;%61;
E = zeros(1, numel(y));
for n = 1:numel(y)-wlen
    for k = 0:wlen-1
        E(n) = E(n) + abs(y(n+k)^2);
    end;
end

e = zeros(1, numel(E));
for k = 1:numel(E)
    if E(k) > 0.18
        e(k) = 1;
    end;
end;

wlen = 7000;%5500;%10500;
p = zeros(numel(e), 1);
pom = 1;
bbb = false;
for n = wlen:1:numel(e)-wlen
    if ~bbb
        s = sum(e(n-wlen+1:n));
    else
        s = sum(e(n:n+wlen));
    end;
    if s ~= 0
        p(n) = 1;
        bbb=true;
    end;
    pom = n;
end;

noise = zeros(numel(y) - nnz(p), 1);
id = 1;
for k = 1:numel(p)
    if p(k) == 0
        noise(id) = y(k);
        id = id + 1;
    end;
end;

figure;
subplot(2,1,1);
t = linspace(0,numel(y)/Fs8kHz, numel(y));
plot(t, y);
xlim([0, numel(y)/Fs8kHz]);
title('Time course of input signal', 'Interpreter', 'latex');
xlabel('Time (s)', 'Interpreter', 'latex')
ylabel('Amplitude', 'Interpreter', 'latex');
subplot(2,1,2);
plot(p, 'r', 'linewidth', 2);
ylim([0, 1.5]);
xlim([0, numel(y)]);
title('Simple speech/noise detection', 'Interpreter', 'latex');
xlabel('Sample (k)', 'Interpreter', 'latex')
ylabel('p(k)', 'Interpreter', 'latex');

audiowrite('noise.wav', noise, Fs8kHz);

%% Denoising signal with spectral subtraction method
[y_nonoise, N] = spectral_subtraction(y, 8, 8980, 60439);
audiowrite('output_no_noise.wav', y_nonoise, Fs8kHz);

figure;
plot_spectrum(N, Fs8kHz);

figure;
y_nonoise = [y_nonoise; zeros(numel(y)-numel(y_nonoise),1)];
t = linspace(0,numel(y)/Fs8kHz,numel(y));
plot(t, y);
hold on;
plot(t, y_nonoise);
xlim([0, numel(y)/Fs8kHz]);
title('Time course of input signal', 'Interpreter', 'latex');
xlabel('Time (s)', 'Interpreter', 'latex')
ylabel('Amplitude', 'Interpreter', 'latex');
legend('original signal', 'denoised signal');

figure;
spectrogram(y_nonoise,1048,[],[],Fs8kHz,'yaxis');
title('Spectrogram of denoised resampled signal with spectral subtraction', 'interpreter', 'latex');
xlabel('Time (s)', 'interpreter', 'latex');
ylabel('Frequency (kHz)', 'interpreter', 'latex');
