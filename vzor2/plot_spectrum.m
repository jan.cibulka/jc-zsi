function [h] = plot_spectrum( x, Fs, varargin )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
%     fs = 1/Fs;
%     t = (0:numel(x)-1)*fs;

%     nfft = 2^nextpow2(numel(x));
%     f = Fs*linspace(0,1,nfft/2 + 1);
%     y = fft(x, nfft)/numel(x);
%     plot(f, 2*abs(y(1:nfft/2 + 1)));
    p = inputParser;
    p.CaseSensitive = false;
    addRequired(p, 'x');
    addRequired(p, 'Fs');
    addOptional(p, 'Type', 'Positive', ...
        @(x) any(validatestring(x, {'Positive', 'Full'})));
    parse(p, x, Fs, varargin{:});
    full = strcmp(p.Results.Type, 'Full');

    dt = 1/Fs;                     % seconds per sample
    StopTime = numel(x)/Fs;                  % seconds
    t = (0:dt:StopTime-dt)';
    N = size(t,1);


    X = fftshift(fft(x));

    dF = Fs/N;                      % hertz
    f = -Fs/2:dF:Fs/2-dF;           % hertz
   
    if full
        h = plot(f,abs(X)/N);
        xlabel('Frequency (Hertz)', 'interpreter', 'latex');
        title('Magnitude Response', 'interpreter', 'latex');
    else
        aX = abs(X)/N;
        h = plot(f(numel(f)/2+1:end), aX(numel(aX)/2+1:end));
        xlabel('Frequency (Hertz)', 'interpreter', 'latex');
        title('Magnitude Response', 'interpreter', 'latex');
    end;
end

