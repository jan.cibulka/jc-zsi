function [ y, h ] = lpfilter( x, Fs, fd, N )
    % FIR filter design
    M = N/2;

    wd = fd/Fs*2*pi;

    % Low-pass FIR filter coefficients
    h = zeros(1,N+1);
    for i=1:N
        h(i+1) = (sin((i-M)*wd))/(pi*(i-M));
    end;
    h(M+1) = 1/2;   % case when i == 0

    % Convolution
    y = zeros(1,numel(x));
    for MAX=1:size(x,1)
        MIN = max(1,MAX-(N));
        y(MAX) = h(1:min(MAX,(N+1))) * x(MIN:MAX);
    end;
end

