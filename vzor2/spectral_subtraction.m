function [ y, D ] = spectral_subtraction( x, N, nstart, nend )
    m = 2^N;
    D = zeros(m,1);
    for i = 1:m:nstart
        D = D + abs(fft(x(i:i+m-1)));
    end
    D = D ./ ceil(nstart/m);

    M = zeros(m,1);
    for i=nend:m:numel(x)-m
        M = M + abs(fft(x(i:i+m-1)));
    end;
    D = D+M ./ (floor((numel(x)-nend)/m));

    alpha = 1.5;
    y = [];
    for i = 1:m:numel(x)-m;
        spectrum = fft(x(i:i+m-1));
        phase = angle(spectrum);
        magnitude = abs(spectrum) - alpha*D;
        magnitude(magnitude < 0) = 0;
        spectrum = magnitude .* exp(1i * phase);
        y = [y; real(ifft(spectrum))];
    end;
end

