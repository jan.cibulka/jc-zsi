function [ y ] = IFFT( x )
    y = conj( FFT( conj(x) ) )/length(x);
end

