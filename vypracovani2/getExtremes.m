function [maxs, mins, zers] = getExtremes(S)
maxs = [];
mins = [];
zers = [];
for i = 2:length(S)-1
	if S(i-1)<0 && S(i)>=0 || S(i-1)>0 && S(i)<=0
		zers(end+1) = i; 
	end
	if S(i-1)<S(i) && S(i)>=S(i+1)
		maxs(end+1) = i;
	elseif S(i-1)>S(i) && S(i)<=S(i+1)
		mins(end+1) = i;
	end
end
maxs(end+1) = length(S);
mins(end+1) = length(S);
end