function IMF = EMD(S, iter, crit)
% EMD - Eempirical Mode Decomposition
%	IMF = EMD(S, iter, crit)
%	S	 - signal: [1,2,3,2,2]
%	iter - number of iterations
%	crit - number of iterations for getting IMF component

fprintf('EMD progress: %5.1f%%\n', 0);
IMF = zeros(iter+1, length(S));

IMF(1,:) = S;
for i = 1:iter
	imf = IMF(i,:);
	for j = 1:1:crit %vyhlazeni imf
		[maxs, mins, ~] = getExtremes(imf);
		splx = spline(maxs, imf(maxs), 1:size(imf,2));
		spln = spline(mins, imf(mins), 1:size(imf,2));
		imf = imf - mean([splx; spln]);
	end
	IMF(i+1,:) = IMF(i,:) - imf; %pripravana dalsi iteraci
	fprintf('\b\b\b\b\b\b\b%5.1f%%\n', i/iter*100);
end
end