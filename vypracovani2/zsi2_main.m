close all;
clear;
clc;

% load signal
file = 'veta3_09.wav';
[S, Fs] = audioread(file);
S = S';
T = 1/Fs; % Sampling period
L = size(S, 2); % Length of signal
t = (0:L-1) * T; % Time vector

plot(Fs * t, S);
title('Voice signal');
xlabel('Signal samples');
ylabel('Amplitude');

% find local extremes and zero crossings
[maxs, mins, zers] = getExtremes(S);

% %%	3.�S�vyu�it�m�funkce�z�bodu�2�implementujte�empirickou�mod�ln��dekompozici
% %	s�omezuj�c�m krit�riem,�kter�m�bude�p�edem�zvolen� maxim�ln��po�et iterac�
% %	nalezen��IMF.
% 
iterations = 9;

IMF = EMD(S, iterations,10);
% figure;	surf(IMF,'MeshStyle','row');
% title('IMF components');
% xlabel('Signal samples');
% ylabel('IMFs');
% zlabel('Amplitude');
% view([1 1 1]);
% 
%  %4.�Porovn�n� spekter IMF
% 
% figure; hold on;
% leg = cell(iterations+1,1);
% leg{1} = 'Base signal';
% maxFreq = 60000;
% for i = 1:iterations+1
% 	f = fft(IMF(i,:))/(length(IMF(i,:))/2);
% 	a = abs(f(1:length(f)/2));
% 	plot(1:length(f)/2, a)
% 	plot(1:maxFreq, abs(f(1:maxFreq)))
% 	if i > 1; leg{i} = sprintf('IMF %d', i-1); end
% end
% title('Spektra komponent');
% xlabel('Frekvence [Hz]');
% ylabel('Amplituda');
% legend(leg);
% hold off;
% 
% %5.okam�it� frekvence
% 
hilb = zeros(iterations+1, length(S)-1);
h_var = zeros(iterations+1, 1);
for i = 1:iterations+1
	hilb(i,:) = diff(unwrap(angle(hilbert(IMF(i,:)))));
	h_var(i,:) = var(hilb(i,:));
end

figure; surf(hilb, 'MeshStyle', 'row');
title('Okam�it� frekvence jednotliv�ch komponent')
xlabel('Zaznam')
ylabel('Komponenty')
zlabel('Frekvence')
view([1 1 1])
