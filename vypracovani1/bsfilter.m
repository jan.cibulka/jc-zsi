function [ y, h ] = bsfilter( x, Fs, fd, fh, N )
    % FIR filter design
    M = N/2;

    wd = fd/Fs*2*pi;
    wh = fh/Fs*2*pi;

    % Band-stop FIR filter coefficients
    h = zeros(1,N+1);
    for i=1:N
    	h(i+1) = (sin((i-M)*wd)-sin(wh*(i-M)))/(pi*(i-M));
    end;
    h(M+1) = 1-1/pi*(wh-wd);    % case when i == 0
    
    % Convolution
    y = zeros(1,numel(x));
    for k=1:numel(x)
    	y(k) = h(1:min(k,N+1)) * x(max(1,k-N):k);
    end;
end

