function [ X ] = FFT( x )
    n = nextpow2(numel(x));
    x = [x; zeros(2^n - numel(x),1)];

    N = numel(x);
    x_odd = x(1:2:end);
    x_even = x(2:2:end);

    if N>=8 
        X_odd = FFT(x_odd);
        X_even = FFT(x_even);
        X = zeros(N,1);

        Wn = exp(-1i*2*pi*((0:N/2-1)')/N);
        k = Wn .* X_even;
        X = [(X_odd + k);(X_odd -k)];
    else
        switch N
            case 2 
                X = [1 1;1 -1]*x;
            case 4 
                X = [1 0 1 0; 0 1 0 -1i; 1 0 -1 0;0 1 0 1i]*[1 0 1 0;1 0 -1 0;0 1 0 1;0 1 0 -1]*x;
            otherwise
                error('Signal x must be power of 2!');
        end;
    end;
end
