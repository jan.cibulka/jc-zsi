function [ y ] = resample( x, L, M )
    load('iir_lp_elliptic');
    y_iir = filter(iir_lp_elliptic, x);
    
    y_over = zeros(1, numel(y_iir)*M);
    v = linspace(0,1,M);
    j = 1;
    for k = 1:numel(y_iir)-1
        y_over(j:j+M-1) = y_iir(k) + v.*(y_iir(k+1)-y_iir(k));
        j = j+M;
    end;

    % Downsample
    y = zeros(floor(numel(y_over)/L),1);
    for k = 1:numel(y)
        y(k) = y_over(k*L);
    end;
end

