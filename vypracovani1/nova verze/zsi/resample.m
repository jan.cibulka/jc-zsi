function [ y ] = resample( x, L, M )
    y_over = zeros(1, numel(x)*M);
    v = linspace(0,1,M);
    j = 1;
    for k = 1:numel(x)-1
        y_over(j:j+M-1) = x(k) + v.*(x(k+1)-x(k));
        j = j+M;
    end;

    % Downsample
    y = zeros(floor(numel(y_over)/L),1);
    for k = 1:numel(y)
        y(k) = y_over(k*L);
    end;
end

