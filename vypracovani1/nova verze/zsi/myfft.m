function X = myfft(x)
    N = length(x);
    if N == 1
        X = x * exp(-2j*pi*1/N);
    else
        M = N/2;
        x_e = x(1:2:N);
        x_o = x(2:2:N);
        X_e = myfft(x_e);
        X_o = myfft(x_o);
        X = zeros(1, N);
        for n = 1:M
            X(n) = X_e(n) + X_o(n) * exp(2i*pi*(n)/N);
        end
        for n = M+1:N
            X(n) = X_e(n-M) - X_o(n-M) * exp(2i*pi*(n-M)/N);
        end
    end
end