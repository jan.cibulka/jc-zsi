%% ZSI - Signal Processing
% Semestral Work I
% Author: Bc. Jan Hranicka

close all;

%% Signal analysis
[y, Fs] = audioread('veta3_09.wav');
fs = 1/Fs;      % Sample period

% t = linspace(0, numel(y)/Fs, numel(y));
% plot(t, y);
% grid on;
% title('Time course of input signal', 'Interpreter', 'latex');
% xlabel('Time (s)', 'Interpreter', 'latex')
% ylabel('Amplitude', 'Interpreter', 'latex');
% xlim([0, numel(y)/Fs]);
% 
% % figure;
% % subplot(2,1,1);
% % plot_spectrum(y, Fs);
% % subplot(2,1,2);
% % spectrogram(y,1048,[],[],Fs,'yaxis');
% 
% % %% FIR - Low-pass filter
lowpass=fir1(100, [0.18],'low');
[y_out_lp, h_lp] = filter(lowpass, 1, y);
audiowrite('output_lp.wav', y_out_lp, Fs)
% % 
% figure;
% subplot(2,1,1);
% spectrogram(y,1048,[],[],Fs,'yaxis');
% title('Spectrogram of original signal', 'interpreter', 'latex');
% xlabel('Time (s)', 'interpreter', 'latex');
% ylabel('Frequency (kHz)', 'interpreter', 'latex');
% subplot(2,1,2);
% spectrogram(y_out_lp,1048,[],[],Fs,'yaxis');
% title('Spectrogram of filtered signal with FIR low-pass filter', 'interpreter', 'latex');
% xlabel('Time (s)', 'interpreter', 'latex');
% ylabel('Frequency (kHz)', 'interpreter', 'latex');
% 
% figure;
% plot_spectrum(y,Fs, 'Positive');
% hold on;
% plot_spectrum(y_out_lp, Fs, 'Positive');
% legend('original signal', 'FIR low-pass filtered');

% % FIR - Band-stop filter
% bandstop=fir1(100,[0.19,0.2177],'stop');
% [y_out_bs, h_bs] = filter(bandstop,1,y);
% [y_out_bs, h_bs] = filter(bandstop,1,y_out_bs);
% [y_out_bs, h_bs] = filter(bandstop,1,y_out_bs);
% [y_out_bs, h_bs] = filter(bandstop,1,y_out_bs); %kdyz to jednou nestaci... jeden ma utlum jen -9db
% audiowrite('output_bs.wav', y_out_bs, Fs)
% 
% figure;
% subplot(2,1,1);
% spectrogram(y,1048,[],[],Fs,'yaxis');
% title('Spectrogram of original signal', 'interpreter', 'latex');
% xlabel('Time (s)', 'interpreter', 'latex');
% ylabel('Frequency (kHz)', 'interpreter', 'latex');
% subplot(2,1,2);
% spectrogram(y_out_bs,1048,[],[],Fs,'yaxis');
% title('Spectrogram of filtered signal with FIR band-stop filter', 'interpreter', 'latex');
% xlabel('Time (s)', 'interpreter', 'latex');
% ylabel('Frequency (kHz)', 'interpreter', 'latex');
% 
% figure;
% plot_spectrum(y, Fs, 'Positive');
% hold on;
% plot_spectrum(y_out_bs, Fs, 'Positive');
% legend('original signal', 'FIR band-stop filtered');
% 
% figure;
% plot_spectrum(y,Fs, 'Positive')
% hold on;
% plot_spectrum(y_out_lp,Fs, 'Positive')
% plot_spectrum(y_out_bs,Fs, 'Positive')
% legend('original signal','FIR low-pass filtered','FIR band-stop filtered');
% 
% 
% fvtool(lowpass, 1);
% fvtool(bandstop, 1);
% 

% 
%% FIR - filters with different kinds of window function


% Hann = fir1(100,[0.17,0.25],'stop',hann(101));
% Hamming = fir1(100,[0.17,0.25],'stop',hamming(101));
% Blackman = fir1(100,[0.17,0.25],'stop',blackman(101));
% 
% y_hann = filter(Hann,1, y);
% y_hamming = filter(Hamming,1, y);
% y_blackman = filter(Blackman,1, y);
% 
% fvtool(Hann, 1);
% fvtool(Hamming, 1);
% fvtool(Blackman, 1);
% 
% % Spectrograms
% figure;
% subplot(3,1,1);
% spectrogram(y_hann,1048,[],[],Fs,'yaxis');
% title('Spectrogram of filtered signal with FIR band-stop Hann filter', 'interpreter', 'latex');
% xlabel('Time (s)', 'interpreter', 'latex');
% ylabel('Frequency (kHz)', 'interpreter', 'latex');
% subplot(3,1,2);
% spectrogram(y_hamming,1048,[],[],Fs,'yaxis');
% title('Spectrogram of filtered signal with FIR band-stop Hamming filter', 'interpreter', 'latex');
% xlabel('Time (s)', 'interpreter', 'latex');
% ylabel('Frequency (kHz)', 'interpreter', 'latex');
% subplot(3,1,3);
% spectrogram(y_blackman,1048,[],[],Fs,'yaxis');
% title('Spectrogram of filtered signal with FIR band-stop Blackman filter', 'interpreter', 'latex');
% xlabel('Time (s)', 'interpreter', 'latex');
% ylabel('Frequency (kHz)', 'interpreter', 'latex');
% 
% % Spectrums
% figure;
% subplot(3,1,1);
% plot_spectrum(y_hann, Fs);
% subplot(3,1,2);
% plot_spectrum(y_hamming, Fs);
% subplot(3,1,3);
% plot_spectrum(y_blackman, Fs);
% legend('Filtered with Hann window', 'Filtered with Hamming window', 'Filtered with Blackman window');

% 
% %% IIR filters design
% close all;
% [Butterworth_a, Butterworth_b] = butter(2, [0.17,0.25], 'stop');
% [Chebyshev_a, Chebyshev_b] = cheby1(2,5, [0.17,0.25], 'stop');
% [IChebyshev_a, IChebyshev_b] = cheby2(10,40, [0.17,0.25], 'stop');
% [Elliptic_a,Elliptic_b] = ellip(2,5,60, [0.17,0.25], 'stop');
% 
% 
% [h1,w1] = freqz(Butterworth_a, Butterworth_b);
% [h2,w2] =freqz(Chebyshev_a, Chebyshev_b);
% [h3,w3] =freqz(IChebyshev_a, IChebyshev_b);
% [h4,w4] =freqz(Elliptic_a, Elliptic_b);
% 
% figure;
% hold on;
% grid on;
% plot(w4/pi,20*log10(abs(h4))) %change h# for different filters
% ax = gca;
% ax.YLim = [-100 20];
% ax.XTick = 0:.1:2;
% title('Elliptic filter');
% xlabel('Normalized Frequency (\times\pi rad/sample)')
% ylabel('Magnitude (dB)')
% 
% y_butterworth = filter(Butterworth_a, Butterworth_b, y);
% y_chebyshev = filter(Chebyshev_a, Chebyshev_b, y);
% y_ichebyshev = filter(IChebyshev_a, IChebyshev_b, y);
% y_elliptic = filter(Elliptic_a,Elliptic_b, y);
% 
% % Spectrograms
% figure;
% subplot(4,1,1);
% spectrogram(y_butterworth,1048,[],[],Fs,'yaxis');
% title('Spectrogram of filtered signal with IIR band-stop Butterworth filter', 'interpreter', 'latex');
% xlabel('Time (s)', 'interpreter', 'latex');
% ylabel('Frequency (kHz)', 'interpreter', 'latex');
% 
% subplot(4,1,2);
% spectrogram(y_chebyshev,1048,[],[],Fs,'yaxis');
% title('Spectrogram of filtered signal with IIR band-stop Chebyshev filter', 'interpreter', 'latex');
% xlabel('Time (s)', 'interpreter', 'latex');
% ylabel('Frequency (kHz)', 'interpreter', 'latex');
% 
% subplot(4,1,3);
% spectrogram(y_ichebyshev,1048,[],[],Fs,'yaxis');
% title('Spectrogram of filtered signal with IIR band-stop Inverse Chebyshev filter', 'interpreter', 'latex');
% xlabel('Time (s)', 'interpreter', 'latex');
% ylabel('Frequency (kHz)', 'interpreter', 'latex');
% 
% subplot(4,1,4);
% spectrogram(y_elliptic,1048,[],[],Fs,'yaxis');
% title('Spectrogram of filtered signal with IIR band-stop Elliptic filter', 'interpreter', 'latex');
% xlabel('Time (s)', 'interpreter', 'latex');
% ylabel('Frequency (kHz)', 'interpreter', 'latex');
% 
% %% Resampling audio to 8 kHz
Fs8kHz = 8000;
L = 441;
M = 80;
y_down = resample(y_out_lp,L,M);

% % Spectrums
% figure;
% plot_spectrum(y_out_lp,Fs);
% hold on;
% plot_spectrum(y_down, Fs8kHz);
% legend('FIR low-pass filtered signal', 'resampled signal to 8 kHz');

% % Spectrograms
% figure;
% subplot(2,1,1);
% spectrogram(y_out_lp,1048,[],[],Fs,'yaxis');
% title('Spectrogram of FIR low-pass filtered signal', 'interpreter', 'latex');
% xlabel('Time (s)', 'interpreter', 'latex');
% ylabel('Frequency (kHz)', 'interpreter', 'latex');
% subplot(2,1,2);
% spectrogram(y_down,1048,[],[],Fs8kHz,'yaxis');
% title('Spectrogram of signal resampled to 8 kHz', 'interpreter', 'latex');
% xlabel('Time (s)', 'interpreter', 'latex');
% ylabel('Frequency (kHz)', 'interpreter', 'latex');

audiowrite('output_8khz.wav', y_down, Fs8kHz)
 
%% Denoising signal with spectral subtraction method

%%	Remove noise
% Frequency subtraction
N = [y_down(1:5000); y_down(95000:numel(y_down))];
LN = length(N);
piece = 2^8;
windows=floor(LN/piece);
Xns = zeros(windows, piece);
for k = 1:windows
	Xns(k,:) = abs(fft(N((k-1)*piece+1 : (k-1)*piece+piece)));
end
Xn = mean(Xns);
dFn = Fs8kHz/piece;
fn = 0:dFn:Fs8kHz/2-dFn;
alpha = 0.8;
S8f = zeros(1, length(y_down));
for k = 1:(numel(y_down)/piece)
	a = (k-1)*piece+1;
	b = (k-1)*piece+piece;
	x = fft(y_down(a:b));
	pha = angle(x);
	amp = abs(x) - alpha*Xn';
	amp(amp<0) = 0;
	
	S8f(a:b) = real(ifft(amp.*exp(1i*pha)));
end
X8 = fft(y_down);
X8f = fft(S8f);
L8 = size(y_down,1);
dF8 = Fs8kHz/L8;
f8 = 0:dF8:Fs8kHz/2-dF8;
figure;
plot(f8, abs(X8(1:L8/2))/L8); hold on;
plot(f8, abs(X8f(1:L8/2))/L8);
set(gcf, 'Position', [353, 216, 740, 420]);
xlabel('Frekvence [Hz]'); ylabel('Magnituda');
title('Spektrum po spektralnim odectu');
legend('Puvodni', 'Po odectu'); hold off;
audiowrite('spektralni_odecet.wav', S8f, Fs8kHz);
