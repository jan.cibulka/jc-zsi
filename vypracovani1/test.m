%% Denoising signal with spectral subtraction method
[y, Fs] = audioread('veta4B.wav');

[y, ~] = bsfilter(y, Fs, 10700, 11300, 1000);

Fs8kHz = 8000;
L = 441;
M = 80;
y = resample(y,L,M);
y = [y; zeros(8, 1)];

alpha = [0.5, 1.0, 1.25, 1.5, 2.0];
figure;
hold on;
for n = 1:numel(alpha)
    [y_nonoise, N] = spectral_subtraction(y, 8, 8980, 60439, alpha(n));
    y_nonoise = [y_nonoise; zeros(numel(y)-numel(y_nonoise),1)];
    h(n) = plot_spectrum(y_nonoise, Fs8kHz);
    l{n} = sprintf('Denoised signal, alpha = %d', alpha(n));
end;
legend(h(:), l{:});

figure;
for n = 1:numel(alpha)
    subplot(numel(wd),1,n);
    [y_nonoise, N] = spectral_subtraction(y, 8, 8980, 60439, alpha(n));
    y_nonoise = [y_nonoise; zeros(numel(y)-numel(y_nonoise),1)];
    spectrogram(y_nonoise, 1048, [], [], Fs8kHz, 'yaxis');
end;
return;

% wd = [2, 4, 6, 8, 10];
% figure;
% hold on;
% for n = 1:numel(wd)
%     [y_nonoise, N] = spectral_subtraction(y, wd(n), 8980, 60439);
%     y_nonoise = [y_nonoise; zeros(numel(y)-numel(y_nonoise),1)];
%     h(n) = plot_spectrum(y_nonoise, Fs8kHz);
%     l{n} = sprintf('Denoised signal, window = %d', 2^wd(n));
% end;
% legend(h(:), l{:});
% 
% figure;
% for n = 1:numel(wd)
%     subplot(numel(wd),1,n);
%     [y_nonoise, N] = spectral_subtraction(y, wd(n), 8980, 60439);
%     y_nonoise = [y_nonoise; zeros(numel(y)-numel(y_nonoise),1)];
%     spectrogram(y_nonoise, 1048, [], [], Fs8kHz, 'yaxis');
% end;
% 
% figure;
% spectrogram(y_nonoise,1048,[],[],Fs8kHz,'yaxis');
% title('Spectrogram of denoised resampled signal with spectral subtraction', 'interpreter', 'latex');
% xlabel('Time (s)', 'interpreter', 'latex');
% ylabel('Frequency (kHz)', 'interpreter', 'latex');
% return;

%% Test
[y, Fs] = audioread('veta4B.wav');

figure;
hold on;
Ns = [100, 250, 400, 500, 750, 900, 1000, 1200];
cmap = hsv(numel(Ns));
for n = 1:numel(Ns)
    [y_out, ~] = bsfilter(y, Fs, 10700, 11300, Ns(n));
    h(n) = plot_spectrum(y_out, Fs);
    set(h(n), 'Color', cmap(n, :));
    l{n} = sprintf('FIR band-stop with N=%d', Ns(n));
end;
legend(h(:), l{:});